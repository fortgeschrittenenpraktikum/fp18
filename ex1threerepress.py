import numpy as np
from matplotlib import pyplot as plt
from boolgen import *


genbau1 = Genbaustein('1', 0, 4, 0.7)
genbau2 = Genbaustein('2', 0, 4, 0.7)
genbau3 = Genbaustein('3', 0, 4, 0.7)

genbau1.connect(-1, genbau2)
genbau2.connect(-1, genbau3)
genbau3.connect(-1, genbau1)

x, U, A = run(0, 100, 1000, [genbau1, genbau2, genbau3])

plt.plot(x, U[genbau1], 'o', label = '1')
plt.plot(x, U[genbau2], 'o', label = '2')
plt.plot(x, U[genbau3], 'o', label = '3')
plt.legend()
plt.show()

plt.plot(x, A[genbau1], 'o', label = '1')
plt.plot(x, A[genbau2], 'o', label = '2')
plt.plot(x, A[genbau3], 'o', label = '3')
plt.legend()
plt.show()
