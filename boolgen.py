import numpy as np
from matplotlib import pyplot as plt

def charging(U_0, U_current, tau, t, dt):
    #U = U_0*(1 - np.exp(-t/tau))
    dU = dt * (U_0 - U_current) * np.exp(-t/tau) / tau
    return dU

def discharging(U_0, tau, t, dt):
    #U = U_0*np.exp(-t/tau)
    dU = dt * U_0 * np.exp(-t/tau) / tau
    return dU

class Genbaustein:
    def __init__(self, protein, threshold, RC, hysteresis) -> None:
        self.protein = protein
        self.threshold = threshold
        self.RC = RC
        self.hysteresis = hysteresis
        self.num_incomming = 0
        self.sending = False
        self.current_charge = 0
        self.start_charge = self.current_charge
        self.sourcecharge = 9
        self.connected = []
    def connect(self, signal_amp, *other):
        """tuple of genbaustein instances to send signal to
        """
        for genbau in (other):
            self.connected.append((genbau, signal_amp))
    def signal(self):
        """sends the signal
        """
        if not self.sending: #check if already sending, so not to much gets send
            for genbau in self.connected: #send signal to every genbaustein to which self is connected
                genbau[0].num_incomming += genbau[1]  
                self.sending = True
    def stopSignal(self):
        if self.sending:
            for genbau in self.connected:
                genbau[0].num_incomming -= genbau[1]
                self.sending = False
    
    def update(self, t, dt):
        if self.num_incomming >= self.threshold and self.current_charge < self.sourcecharge:
            self.current_charge += charging(self.sourcecharge,self.start_charge, self.RC, t, dt)
            self.final_charge = self.current_charge

        elif self.num_incomming < self.threshold and self.current_charge > 0:
            self.current_charge -= discharging(self.final_charge, self.RC, t, dt)
            self.start_charge = self.current_charge

        if self.current_charge > self.sourcecharge * (0.5 + 0.5*self.hysteresis):
            self.signal()
        
        if self.current_charge < self.sourcecharge * (0.5 - 0.5*self.hysteresis):
            self.stopSignal()

def create_adjacency_mat(genbaus):
    #TODO
    adjacencymat = np.zeros((len(genbau), len(genbau)))

def run(start, end, N, genbaus):
    U = {}
    A = {}
    time_new = {}
    for genbau in genbaus:
        U[genbau] = []
        A[genbau] = []
        time_new[genbau] = 0

    x = np.linspace(start, end, N)
    for time in x:
        for genbau in genbaus:
            U[genbau].append(genbau.current_charge)
            A[genbau].append(genbau.num_incomming)



        for genbau in genbaus:
            genbau.update(time-time_new[genbau], (end-start)/N)
        
        for genbau in genbaus:
            if A[genbau][-1] != genbau.num_incomming:
                time_new[genbau] = time
        
    return x, U, A

